//production credentials 
if (process.env.NODE_ENV === 'production') {
    module.exports = {
        JWT_SECRET: process.env.JWT_SECRET,
        MONGO_URI: process.env.MONGO_URI,
    };
}
//dev credentials
if (process.env.NODE_ENV === 'development') {
    module.exports = {
        JWT_SECRET: 'fdksjfqoj399392h3kdj',
        MONGO_URI: "mongodb://api:testpassword1@ds016128.mlab.com:16128/fmg_coding_challenge",
    };
}

