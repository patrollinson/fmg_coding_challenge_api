const passport = require('passport');
const JwtStrategy = require('passport-jwt').Strategy;
const { ExtractJwt } = require('passport-jwt');
const LocalStrategy = require('passport-local').Strategy;
const config = require('./config/index');
const Client = require('./models/client');


//validating JWT, 
//valid JWT, create req.user object with simple client obj with clientID,
//in future can search for client and add it to the req.user obj
passport.use(new JwtStrategy({
    jwtFromRequest: ExtractJwt.fromHeader('authorization'),
    secretOrKey: config.JWT_SECRET
}, async (payload, done) => {
    try {
        let client = {
            clientId: payload.sub,
        }
        done(null, client);
    } catch (error) {
        console.log('Error JWT')
        done(error, false);
    }
}))

