var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var morgan = require('morgan');

var bodyParser = require('body-parser');
var mongoose = require('./mongoose');
const cors = require('cors');

//routes
var clientApi = require('./routes/api/client');

var app = express();

//mongoose
var db = mongoose.connect();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(morgan('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));

//profile pics static
app.use('/uploads', express.static('uploads'));
app.use(cors())

//app routes
app.use('/api/client', clientApi);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  //res.render('error');
});

module.exports = app;


