var path = require('path');
const EmailTemplate = require('email-templates')
const nodemailer = require('nodemailer');
//creating transport, with hard coded credentials
const adminTransport = nodemailer.createTransport({
    host: 'smtp.zoho.com',
    port: 465,
    secure: true,
    auth: { 
        user: 'notifications@connectedsystems.co.za',
        pass: 'uppereastside#2018',
    },
});

//adding pug template 
const adminEmail = new EmailTemplate({
    message: {
        from: 'notifications@connectedsystems.co.za',
    },
    views: {
        root: path.join(__dirname, 'templates')
    },
    
    send: true,
    transport: adminTransport
})

module.exports = {
    sendResetPassword(email, clientId){
        return new Promise(function(resolve, reject){
            var emmSetupConfirmationSent = adminEmail.send({
                message: {
                    to: email
                },
                template: 'resetPassword',  
                locals: {
                    linkUrl: 'http://localhost:8000/#!/resetPassword/' + clientId 
                }
            })
            .then(
                //if success return true
                resolve(true)
            )
            .catch( 
                //if error, return false
                reject(false)
            )
        })
        
    }
   
}
