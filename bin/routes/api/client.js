/////////   http://3000/api/api/client/..... routes   ////////

const express = require('express');
const router = require('express-promise-router')();

//passport for token validation
const passport = require('passport');
const passportConf = require('../../passport');
const passportJWT = passport.authenticate('jwt', { session: false });

//controllers
const ClientController = require('../../controllers/clientController');
const AuthClient = require('./../../authenticate/client')

//profile pic uploads
const multer = require('multer');
const storage = multer.diskStorage({
    destination: function(req, file, cb){
        cb(null, './uploads');
    },
    filename: function(req, file, cb){
        cb(null, new Date().toISOString() + file.originalname);
    }
});

const fileFilter = function(req, file, cb){
    if(file.mimetype === 'image/jpeg' || file.mimetype === 'image/png'){
        cb(null, true);
    }
    else{
        cb(null, false);
    }
}

const upload =  multer({
    storage: storage,
    limits: {fieldSize : 1024 * 1024 * 5}, //max file size 5MB
    fileFilter: fileFilter
});

////////////////////////////////AUTHENTICATION CONTROLLER///////////////////////////
router.route('/signup')
    .post(
        upload.single('profile_pic'),  //upload profile_pic to uploads file
        AuthClient.signup
    ); 

router.route('/signin')
    .post(
        AuthClient.signin
    );

////////////////////////////////////CLIENT CONTROLLER///////////////////////////////
router.route('/:clientId')
    .get(
        passportJWT, 
        ClientController.getClientInfo
    );    

router.route('/:clientId/edit')
    .post(
        passportJWT, 
        ClientController.updateClientInfo
    );   
    
router.route('/:clientId/log/:page')
    .get(
        passportJWT, 
        ClientController.getClientLog
    );   
    
router.route('/:clientId/sendResetPassword')
    .get(
        passportJWT, 
        ClientController.sendResetPassword
    ); 

router.route('/:clientId/resetPassword')
    .post(
        //unprotected route, need to improve security
        ClientController.resetPassword
    ); 

module.exports = router;
