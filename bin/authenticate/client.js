////  AUTHENICATION CONTROLLER    ////
const Client = require('../models/client');
const passport = require('passport');
const passportSignIn = passport.authenticate('local', { session: false });
const JWT = require('jsonwebtoken');
const { JWT_SECRET } = require('../config/index');


//signing client token
const signToken = (client) => {
    let payload = {
        "iss": 'codingChallengeApiV1.1',
        "sub": client._id,
        "client": true,
        "admin": false,
        "iat": Date.now() / 1000,
        "exp": Math.floor(Date.now() / 1000) + (60 * 60 * 8)  //expires after 8 hours
    }
    return JWT.sign(payload, JWT_SECRET);
};

module.exports = {
    signin: async (req, res, next) => {

        let email = req.body.email;
        let password = req.body.password;

        let client = await Client.findOne({ email })
        
        if (client) {
            let clientId = client._id.toString()
            //check if password is correct 
            const isMatch = await client.isValidPassword(password);
            //if not handle it 
            if (!isMatch) {
                res.status(200).json({ error: "Wrong password" })
            }
            //password correct
            if (isMatch) {
                //adding sign-in activity to activity log
                const updatedClient = await Client.findByIdAndUpdate(
                    clientId,
                    {
                        $push: {
                            'activity_log':
                                { 'activity': 'sign-in' }
                        }
                    },
                    function (err, c) {
                        if (err) {
                            //unable to log activity -> unhandled error, but still log in client 
                            console.log('err:', err);
                            res.status(200).json({ token, client });
                        }
                        else {
                            //clients logs in
                            const token = signToken(client);
                            res.status(200).json({ token, client });
                        }
                    }
                );
            }
        }
        else {
            res.status(200).json({ error: "Wrong email" })
        }
    },

    signup: async (req, res, next) => {
        const email = req.body.email;

        //check if there is a client with same email address
        const foundClient = await Client.findOne({ email: email });
        
        console.log('foundClient: ', foundClient);
        if (foundClient) {
            //email address used 
            console.log("Sign Up: Found Email");
            res.status(409).json({ error: "Email is already used" })
        }
        else {
            //create new client
            const newClient = new Client({
                email: req.body.email,
                password: req.body.password,
                first_name: req.body.first_name,
                last_name: req.body.last_name,
                favourite_colour: req.body.favourite_colour,
                profile_pic_url: req.file.path
            });

            const newClientMade = await newClient.save(function (err, client) {
                if (client) {
                    res.status(200).json(client);
                }
                else {
                    res.status(400).json({ message: 'Error' });
                }
            })
        }
    }
}

