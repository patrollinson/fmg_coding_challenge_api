const JWT = require('jsonwebtoken');
const { JWT_SECRET } = require('../config/index');
const notificationEmail = require('../email/notifications');
const Client = require('../models/client');

module.exports = {
    //profile 
    getClientInfo: async (req, res, next) => {
        const clientInfo = await Client.findById(req.params.clientId).select('-password');
        if (!clientInfo) {
            return res.status(404).json({ error: "Client details not found" })
        }
        res.status(200).json({ clientInfo })
    },
    //edit profile
    updateClientInfo: async (req, res, next) => {
        //find client 
        const newClientAuth = await Client.findById(req.params.clientId);
        //update fields
        newClientAuth.first_name = req.body.first_name;
        newClientAuth.last_name = req.body.last_name;
        newClientAuth.favourite_colour = req.body.favourite_colour;
        //save
        updatedClientAuth = await newClientAuth.save()
        if (updatedClientAuth) {
            res.status(200).json(updatedClientAuth)
        }
        else {
            res.status(404).json({ error: "Could not update your details" })
        }
    },

    sendResetPassword: async (req, res, next) => {
        const clientId = req.params.clientId;
        //check if client exists
        const client = await Client.findById(clientId)
        if (client) {
            //if exists, send email, return promise
            const emailSent = notificationEmail.sendResetPassword(client.email, client._id)
                .then(x => { //success
                    res.status(200).json({ message: 'An email containing a link to reset your password has been sent to your email address. Please follow the instructions given in it.' });
                })
                .catch(y => { //error
                    res.status(400).json({ message: 'Error sending reset password email' })
                })
        }
        else {
            //no client, return error
            res.status(400).json({ message: 'Error sending reset password email' })
        }
    },

    resetPassword: async (req, res, next) => {
        const clientId = req.params.clientId;
        //check if client exists
        const client = await Client.findById(clientId);
        //update password field
        client.password = req.body.password;
        //save (hash password)
        client.save(function (err, x) {
            if (err) {
                res.status(200).json({ message: 'Error updating your password' })
            }
            else {
                res.status(200).json({ message: 'Your password has been updated, please sign in with your new password.' })
            }
        })
    },

    getClientLog: async (req, res, next) => {
        const logPerPage = 5;           //number of logged activities per page
        const page = req.params.page;   //requested page (page 1 = past 0 to 5 events, page 2 = past 6 to 10 events)
        //return all acitivity logs
        const clientLogs = await Client.findById(req.params.clientId).select('activity_log');
        if (!clientLogs) {
            return res.status(404).json({ error: "Client Energy monitoring modules could not be found" });
        }
        else {
            var activityLog = [];       //to be returned to client
            var numLogs = { total: clientLogs.activity_log.length } //total number of event, for pagination on client side

            for (let i = 0; i < clientLogs.activity_log.length; i++) {
                if (Math.floor(i / logPerPage) === page - 1) {  ///page = 1, then will return 0 to 5 etc
                    activityLog.push(clientLogs.activity_log[i]) //push to array
                }
            }
            //return correct logs, and total number
            res.status(200).json({ activityLog, total: clientLogs.activity_log.length });
        }
    }
}
