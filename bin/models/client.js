const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');

const Schema = mongoose.Schema;

const ActivityLogSchema = Schema(
    {
        ts: {type: Date, default: Date.now},
        activity: {
            type: String, 
            required: true, 
            max: 100
        },
    }
)

const ClientSchema = Schema(
    {
        email: {
            type: String, 
            max: 100, 
            required: true, 
            unique: true,
            lowercase: true
        },
        password: {
            type: String, 
            required: true
        },
        first_name: {
            type: String, 
            required: true, 
            max: 100
        },
        last_name: {
            type: String, 
            required: true, 
            max: 100
        },
        favourite_colour: {
            type: String, 
            required: true, 
            max: 100
        },
        profile_pic_url: {
            type: String, 
            required: true, 
            max: 100
        },
        activity_log: [ActivityLogSchema]
    }
);

//runs function when save is called, hashes password field
ClientSchema.pre('save', async function(next){
    try{ 
        //generate salt
        const salt = await bcrypt.genSalt(10);
        //generate a password hash (password + hash)
        const passwordHash = await bcrypt.hash(this.password, salt);
        this.password = passwordHash;
        next();
    }catch(error){
        next(error)
    }  
});

//validates password
ClientSchema.methods.isValidPassword = async function(newPassword){
    try{
        return await bcrypt.compare(newPassword, this.password)
    } catch(error){
        throw new Error(error)
    }
}

//Export model
module.exports = mongoose.model('Client', ClientSchema);
        