'use strict'
const mongoose = require('mongoose');
const config = require('./config/index');
mongoose.Promise = global.Promise;

module.exports.connect = function(cb){
    console.log(process.env.NODE_ENV);
    mongoose.connect(config.MONGO_URI, {useMongoClient: true})

    const db = mongoose.connection;
    db.on('error', console.error.bind(console, 'Mongo connection error'));
    db.on('connected', console.log.bind(console, 'Mongo connection success'));
    //db.once('open', console.log('Mongo connection success')); 
    return db;
  };