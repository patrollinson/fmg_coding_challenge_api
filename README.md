//FMG Coding Challenge API

These instruction only apply to the API

Setup:
1. Clone Project into directory
2. In console/terminal run the following:
    - 'npm install' -to install all dependancies
    - 'npm run dev' -to start API, if running correctly should say the following: 
        [nodemon] 1.14.11
        [nodemon] to restart at any time, enter `rs`
        [nodemon] watching: *.*
        [nodemon] starting `node server.js`
        development
        Mongo connection success
